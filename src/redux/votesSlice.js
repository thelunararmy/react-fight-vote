import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const getStartingVotes = createAsyncThunk(
  "votes/getStartingVotes",
  async () => {
    const response = await fetch(
      `https://randomuser.me/api/?results=50&password=lower,1-1`
    );
    if (response.ok) {
      const userData = await response.json();
      return { userData };
    }
  }
);

export const getVotesFromAPI = createAsyncThunk(
  "votes/getVotesFromAPI",
  async (payload) => {
    const response = await fetch(
      `https://randomuser.me/api/?results=${payload.count}&password=lower,1-1`
    );
    if (response.ok) {
      const userData = await response.json();
      return { userData };
    }
  }
);

const processVoteData = (state, action) => {
  action.payload.userData.results.forEach((vote) => {
    const data = {
      username: vote.name.first + " " + vote.name.last,
      vote: "bdfhjlortvxz".includes(vote.login.password) ? "musk" : "zuck",
      img: vote.picture.thumbnail,
    };
    state.userVotes.push(data);
    if (data.vote === "musk") {
      state.musk += 1;
    } else {
      state.zuck += 1;
    }
  });
};

export const votesSlice = createSlice({
  name: "votes",
  initialState: {
    zuck: 0,
    musk: 0,
    userVotes: [], // { username, whovoted }
  },
  reducers: {
    incrementZuck: (state) => {
      state.zuck += 1;
    },
    incrementMusk: (state) => {
      state.musk += 1;
    },
  },
  extraReducers: {
    [getStartingVotes.fulfilled]: processVoteData,
    [getVotesFromAPI.fulfilled]: processVoteData,
  },
});

export const { incrementZuck, incrementMusk } = votesSlice.actions;
export const votesReducer = votesSlice.reducer;
