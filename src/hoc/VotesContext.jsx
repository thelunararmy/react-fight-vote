import { createContext, useState } from "react";

export const VotesContext = createContext(null);

const VotesProvider = ({ children }) => {
  const [votes, setVotes] = useState({ musk: 3, zuck: 7 });

  return (
    <VotesContext.Provider value={[votes, setVotes]}>
      {children}
    </VotesContext.Provider>
  );
};

export default VotesProvider;
