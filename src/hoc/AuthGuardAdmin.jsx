import { NavLink } from "react-router-dom";
import keycloak from "../keycloak/keycloak";

const AuthGuardAdmin = ({ children }) => {
  if (keycloak.authenticated) {
    if (keycloak.hasRealmRole("ADMIN")) {
      return <>{children}</>;
    } else {
      return (
        <div>
          <h3>You are not authorized to view this page.</h3>
          <h4>
            Your account lacks the proper role. Contact the keycloak admin.
          </h4>
          <NavLink to="/">Return Home 🏠</NavLink>
        </div>
      );
    }
  } else {
    return (
      <div>
        <h3>You are not authorized to view this page.</h3>
        <h4>Please login and try again</h4>
        <NavLink to="/">Return Home 🏠</NavLink>
      </div>
    );
  }
};
export default AuthGuardAdmin;
