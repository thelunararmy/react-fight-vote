import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import "./App.css";
import FightStatistic from "./components/FightStatistic";
import UserVotes from "./components/UserVotes";
import VotesProvider from "./hoc/VotesContext";
import NotFound from "./components/NotFound";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { getStartingVotes } from "./redux/votesSlice";
import keycloak from "./keycloak/keycloak";
import TokenPage from "./components/TokenPage";
import AuthGuardAdmin from "./hoc/AuthGuardAdmin";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getStartingVotes());
  }, [dispatch]);

  return (
    <BrowserRouter>
      <VotesProvider>
        <div className="App">
          <header>
            <img
              src="./header_image.png"
              height="100px"
              alt="header_image"
            ></img>
            <h1>Vote now!</h1>
            <h2>Mark Zuckerberg 🤖 vs 🦨 Elon Musk</h2>
          </header>{" "}
          <nav>
            <li>
              <Link to="/">🏠 Home</Link>
            </li>
            <li>
              <Link to="/votes">📝 Votes</Link>
            </li>
            <li>
              <Link to="/token">👛 Token</Link>
            </li>
            {keycloak.authenticated ? (
              <>
                <li>
                  <span>
                    Hello there, <b>{keycloak.tokenParsed.name}</b>!
                  </span>
                </li>
                <li>
                  <button onClick={() => keycloak.logout()}>Logout</button>
                </li>
              </>
            ) : (
              <li>
                <button onClick={() => keycloak.login()}>Login</button>
              </li>
            )}
          </nav>
          {/* Serve rest of content based on url */}
          <Routes>
            <Route path="/" element={<FightStatistic />} />
            <Route path="/votes" element={<UserVotes />} />
            <Route
              path="/token"
              element={
                <AuthGuardAdmin>
                  <TokenPage />
                </AuthGuardAdmin>
              }
            />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </div>
      </VotesProvider>
    </BrowserRouter>
  );
}

export default App;
