import keycloak from "../keycloak/keycloak";
import "./TokenPage.css";

const TokenPage = () => {
  const token = keycloak.token;
  return <pre>{token}</pre>;
};

export default TokenPage;
