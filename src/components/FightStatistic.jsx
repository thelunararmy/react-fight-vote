import {
  // useContext,
  useState,
} from "react";
// import { VotesContext } from "../hoc/VotesContext";
import { useDispatch, useSelector } from "react-redux";
import { incrementMusk, incrementZuck } from "../redux/votesSlice";

function FightStatistic() {
  // const [votes, setVotes] = useContext(VotesContext);
  const [userVote, setUserVote] = useState("");
  const muskVotes = useSelector((state) => state.votes.musk);
  const zuckVotes = useSelector((state) => state.votes.zuck);
  const dispatch = useDispatch();

  function handleSubmitVote() {
    switch (userVote) {
      case "zuck":
        dispatch(incrementZuck());
        break;
      case "musk":
        dispatch(incrementMusk());
        break;
      default:
        break;
    }
  }

  function handleVoteSelected(event) {
    setUserVote(event.target.value);
  }

  return (
    <div>
      <p>🤖 : {zuckVotes}</p>
      <p>🦨 : {muskVotes}</p>
      <div>
        <div>
          <p>Vote on who you think will win!</p>
          <input
            type="radio"
            value="musk"
            name="musk"
            onChange={handleVoteSelected}
            checked={userVote === "musk"}
          />
          🦨 - Elon Musk
          <br />
          <input
            type="radio"
            value="zuck"
            name="zuck"
            onChange={handleVoteSelected}
            checked={userVote === "zuck"}
          />
          🤖 - Mark Zuckerberg
          <br />
          <p>You have selected: {userVote} !</p>
          <button onClick={handleSubmitVote} disabled={userVote === ""}>
            Submit your vote!
          </button>
        </div>
      </div>
    </div>
  );
}

export default FightStatistic;
