import { useDispatch, useSelector } from "react-redux";
import { getVotesFromAPI } from "../redux/votesSlice";

function convertToEmoji(text) {
  return text === "musk" ? "🦨" : "🤖";
}

const UserVotes = () => {
  const votes = useSelector((state) => state.votes.userVotes);
  const dispatch = useDispatch();
  const handleFetchSingleVote = (count) => {
    dispatch(getVotesFromAPI({ count }));
  };

  return (
    <div>
      <div>
        <button onClick={() => handleFetchSingleVote(1)}>
          Get another Vote
        </button>
        <button onClick={() => handleFetchSingleVote(5)}>Get 5 Votes</button>
      </div>
      <p>User data:</p>
      {votes &&
        votes.map((vote) => {
          return (
            <div>
              <img src={vote.img} width="20px" alt="userpic"></img>
              {vote.username} - Vote: {convertToEmoji(vote.vote)}
            </div>
          );
        })}
    </div>
  );
};

export default UserVotes;
