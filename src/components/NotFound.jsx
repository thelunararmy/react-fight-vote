import { Link } from "react-router-dom";

function NotFound() {
  return (
    <div>
      <p>404. Page not found!</p>
      <Link to="/">Click here to return home</Link>
    </div>
  );
}

export default NotFound;
