# Fight Vote App
A demo app anyone can `git clone` and play around with. Vote for who would win in a blood fight against two billionaires.

## Install
```
git clone https://gitlab.com/thelunararmy/react-fight-vote
cd react-fight-vote
npm install
```

## Usage
```
npm start
``` 
This will open a new Webpage in your browser at `localhost:3000`. Remember to use your React and Redux browser extentions

## Set-up

This particular example doesnt require any setup, but if it were to use any API keys or URLS, then you would need to make a `.env` file using the following template:

```
{
    API_KEY : your key,
    API_URL : resource url,
}
```

I am a smart person who NEVER share those items in a README, so please look for other communication methods to find the key and url 💃

## Contributors

* [JC Bailey (@thelunaramy)](@thelunararmy)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
Noroff Accelerate, 2023.